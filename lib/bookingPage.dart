import 'package:flutter/material.dart';

class bookingPage extends StatelessWidget {
  const bookingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
                Container(
                  height: 380,
                  decoration: const BoxDecoration(image: DecorationImage(image: NetworkImage('https://images.unsplash.com/photo-1548328529-5f3953ceacd9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80'),fit: BoxFit.cover),),
                ),
              Padding(
                padding: const EdgeInsets.only(top: 50,left: 15 ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        InkWell(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: const CircleAvatar(
                            radius: 23,
                            backgroundColor: Colors.white,
                            child: Icon(Icons.arrow_back_ios_new_rounded,color: Colors.black,),
                          ),
                        ),
                      ],
                    ),
                    const Row(
                      children: [CircleAvatar(
                        radius: 23,
                        backgroundColor: Colors.white,
                        child: Icon(Icons.ios_share_rounded,color: Colors.black,size: 30),
                      ),
                        SizedBox(width: 10),
                        CircleAvatar(
                          radius: 23,
                          backgroundColor: Colors.white,
                          child: Icon(Icons.favorite_border_rounded,color: Colors.black,size: 30),
                        ),
                        SizedBox(width: 15),],
                    ),
                  ],
                ),
              ),
              Positioned(
                right: 15,
                bottom: 10,
                child: TextButton(onPressed: null, child: const Text("1/21",style: TextStyle(color: Colors.white),),style: TextButton.styleFrom(
                    backgroundColor: Colors.black,
                    shape:const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                    padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 20)
                ),),
              ),
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left:7,top: 3,bottom: 5),
                      child: TextButton(onPressed: null, child: const Text("Recommended",style: TextStyle(color: Colors.white),),style: TextButton.styleFrom(
                          backgroundColor: Colors.black,
                          shape:const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                          padding: const EdgeInsets.symmetric(vertical: 13,horizontal: 20)
                      ),),
                    ),
                    const Text("Forest Haven Estate - Modern Villa (near mountain)",style: TextStyle(color: Colors.black,fontSize: 28,fontWeight: FontWeight.w700),),
                    const Text("Mistybrook, Oregon, United State",style: TextStyle(fontSize: 18,color: Colors.black26),),
                    const SizedBox(height: 7,),
                    const Row(
                      children: [
                        Icon(Icons.star_border_rounded,size: 30),
                        Text("4,5 Rating",style: TextStyle(fontWeight: FontWeight.w500,fontSize: 15),),
                        SizedBox(width: 10,),
                        Icon(Icons.circle_rounded,size: 7,color: Colors.black26,),
                        SizedBox(width: 7,),
                        Icon(Icons.location_on_outlined,size: 30),
                        Text("1 Km",style: TextStyle(fontWeight: FontWeight.w500,fontSize: 15)),
                        SizedBox(width: 10,),
                        Icon(Icons.circle_rounded,size: 7,color: Colors.black26,),
                        SizedBox(width: 7,),
                        Text("23 Reviews",style: TextStyle(decoration: TextDecoration.underline,decorationColor: Colors.black26,decorationThickness:2,fontWeight: FontWeight.w500) ,),
                      ],
                    ),
                    const SizedBox(height: 15,),
                    const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Facility",style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
                        Text("See all",style: TextStyle(fontSize: 17,fontWeight: FontWeight.w500),)
                      ],
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 90,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: icon.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  width: 100,
                                  padding: const EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black12, width: 2),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  margin: const EdgeInsets.symmetric(horizontal: 4),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      icon[index],
                                      const SizedBox(height: 25),
                                      Text(text[index]),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20,),
                    const Text("Location Address",style:TextStyle(color: Colors.black87,fontSize: 23,fontWeight: FontWeight.w500),),
                    const SizedBox(height: 10,),
                    const Text("123 Mystical Lane, Enchanted Street, Mistybrook, OR 98765",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),),
                  ],
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(15),
            decoration: const BoxDecoration(
              border: Border(top: BorderSide(color: Colors.black12,width: 1)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("From",style: TextStyle(color: Colors.black54),),
                    Row(
                      children: [
                        Text("\$120",style: TextStyle(fontWeight: FontWeight.bold,fontSize:28),),
                        Padding(
                          padding: EdgeInsets.only(top: 9),
                          child: Text(" / Night",style: TextStyle(color: Colors.black54,),),
                        )
                      ],
                    )
                  ],
                ),
                TextButton(onPressed: null, child: const Text("Reserve",style: TextStyle(color: Colors.white,fontSize: 25),),style: TextButton.styleFrom(
                    backgroundColor: Colors.green[800],
                    shape:const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
                    padding: const EdgeInsets.symmetric(vertical: 15,horizontal: 28)
                ),),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

List<dynamic> icon = [const Icon(Icons.bed_outlined),const Icon(Icons.shower_outlined),const Icon(Icons.pool_outlined),const Icon(Icons.outdoor_grill_outlined),const Icon(Icons.accessibility_new_rounded)];
List<dynamic> text = ["2 King Beds","2 Bathroom","Pool","BBQ","DummyText"];

