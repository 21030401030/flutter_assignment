import 'package:assignment/bookingPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class home extends StatelessWidget {
  const home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 50,left: 20,right: 20),
            child: Container(
              padding: const EdgeInsets.only(top:4,left:20,bottom: 4),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black12),
                borderRadius: BorderRadius.circular(50),
              ),
              child: const TextField(
                decoration: InputDecoration(
                  hintText: "Bali, Indonesia",
                  hintStyle: TextStyle(
                    color: Colors.black38
                  ),
                  icon: Icon(Icons.search_outlined,size: 35),
                  border: InputBorder.none,
                  iconColor: Colors.black,
                ),
              ),
            ),
          ),
          const SizedBox(height: 20,),
          Container(
            margin: const EdgeInsets.only(bottom: 0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: List.generate(hIcons.length, (index) {
                    Color? itemColor = index == 0 ? Colors.green[700] : Colors.transparent;
                    Color iconColor = index == 0 ? Colors.white : Colors.black;
                    Color textColor = index == 0 ? Colors.white : Colors.black;
                    return Padding(
                      padding: const EdgeInsets.only(right:10),
                      child: Container(
                        height:45,
                        padding: const EdgeInsets.only(right:20,left: 20),
                        decoration: BoxDecoration(
                          color: itemColor,
                          borderRadius: const BorderRadius.all(Radius.circular(30)),
                          border: Border.all(color: Colors.black12),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Icon(
                              hIcons[index].icon,
                              color: iconColor,
                            ),
                            const SizedBox(width: 10,),
                            Text(hText[index],style: TextStyle(color: textColor)),
                          ],
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ),
          ),
          const SizedBox(height:10 ,),
          Expanded(
            child: ListView.builder(scrollDirection: Axis.vertical,itemCount: 4,itemBuilder: (context,index){
              return InkWell(
                onTap: ()=>{
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>const bookingPage()))
                },
                child: Container(
                  padding: const EdgeInsets.only(left: 20,right: 20,bottom: 13),
                  child: Stack(
                    children: [
                      Container(
                        height: 350,
                        decoration: BoxDecoration(boxShadow: [
                        BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 5,
                        offset: const Offset(0, 3),)],
                          borderRadius: const BorderRadius.all(Radius.circular(20)),image: DecorationImage(image: NetworkImage(hImage[index]),fit: BoxFit.cover),),
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top:10,right: 10,left: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                TextButton(onPressed: null, child: const Text("Recommended",style: TextStyle(color: Colors.white),),style: TextButton.styleFrom(
                                    backgroundColor: Colors.black,
                                    shape:const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
                                    padding: const EdgeInsets.symmetric(vertical: 13,horizontal: 20)
                                ),),
                                const CircleAvatar(
                                  radius: 23,
                                  backgroundColor: Colors.white,
                                  child: Icon(Icons.favorite_border_rounded,color: Colors.black,size: 30),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height:170,),
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Container(
                              decoration: const BoxDecoration(color: Colors.white,borderRadius: BorderRadius.all(Radius.circular(10))),
                              child: const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Forest Haven Estate - Modern Villa",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                                    SizedBox(height: 7,),
                                    Text("MistyBrook, Oregon, United States",style: TextStyle(color: Colors.black38,fontSize: 16)),
                                    SizedBox(height: 7,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Icon(Icons.star_border_rounded,size: 30),
                                            Text("4,5 Rating",style: TextStyle(fontSize: 15),),
                                            SizedBox(width: 10,),
                                            Icon(Icons.circle_rounded,size: 7,color: Colors.black26,),
                                            SizedBox(width: 7,),
                                            Icon(Icons.location_on_outlined,size: 30),
                                            Text("1 Km",style: TextStyle(fontSize: 15)),
                                            SizedBox(width: 10,),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Text("\$120",style: TextStyle(fontWeight: FontWeight.w800,fontSize:24),),
                                            Padding(
                                              padding: EdgeInsets.only(top: 4),
                                              child: Text("/night",style: TextStyle(color: Colors.black54,),),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),

                    ],
                  ),
                ),
              );
            }),
          ),
          Material(
            elevation: 4,
            child: Container(
              height: 75,
              padding: const EdgeInsets.only(top: 10,bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Icon(CupertinoIcons.search,size: 35,color: Colors.green[700],),
                      const SizedBox(height:3,),
                      Text("Search",style: TextStyle(color: Colors.green[700]),),
                    ],
                  ),
                  const Column(
                    children: [
                      Icon(CupertinoIcons.book,size: 35,color: Colors.black38),
                      SizedBox(height:3,),
                      Text("Trip",style: TextStyle(color: Colors.black38)),
                    ],
                  ),
                  const Column(
                    children: [
                      Icon(CupertinoIcons.heart,size: 35,color: Colors.black38),
                      SizedBox(height:3,),
                      Text("Wishlist",style: TextStyle(color: Colors.black38)),
                    ],
                  ),
                  const Column(
                    children: [
                      Icon(CupertinoIcons.chat_bubble_text,size: 35,color: Colors.black38),
                      SizedBox(height:3,),
                      Text("Message",style: TextStyle(color: Colors.black38)),
                    ],
                  ),
                  InkWell(
                  onTap: () => Navigator.pop(context),
                    child: const Column(
                    children: [
                      Icon(CupertinoIcons.profile_circled,size: 35,color: Colors.black38),
                      SizedBox(height:3,),
                      Text("Profile",style: TextStyle(color: Colors.black38),),
                    ],
                  ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

List<dynamic> hIcons = [const Icon(Icons.home_work_sharp),const Icon(Icons.car_rental),const Icon(Icons.restaurant),const Icon(Icons.accessibility_new_rounded)];
List<dynamic> hText = ["Hotel","Rentals","Restuarant","Dummy Text"];
List<dynamic> hImage = ["https://images.unsplash.com/photo-1548328529-5f3953ceacd9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80","https://images.unsplash.com/photo-1546476472-888994447a5d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=764&q=80",
  "https://images.unsplash.com/photo-1518780664697-55e3ad937233?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=765&q=80","https://images.unsplash.com/photo-1502304104451-b61947b321ca?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1yZWxhdGVkfDJ8fHxlbnwwfHx8fHw%3D&w=1000&q=80",
];
