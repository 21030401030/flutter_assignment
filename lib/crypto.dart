import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Crypto extends StatelessWidget {
  const Crypto({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20,right: 20,left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            backgroundImage: AssetImage("assets/images/person.jpg"),
                          ),
                          SizedBox(
                            width: 17,
                          ),
                          Text("Owen",style: TextStyle(fontSize: 17,fontWeight: FontWeight.w400),),
                          Icon(Icons.keyboard_arrow_right_rounded,color: Colors.black45,)
                        ],
                      ),
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Icon(
                          Icons.notifications_none_rounded,color: Colors.black,size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 25),
                Container(
                  padding: const EdgeInsets.only(left: 17, top: 12, bottom: 12),
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(25)),
                  child: const TextField(
                    decoration: InputDecoration(
                        hintText: "Search",
                        hintStyle: TextStyle(color: Colors.black38, fontSize: 20),
                        prefixIcon: Icon(Icons.search_sharp, size: 39),
                        border: InputBorder.none),
                  ),
                ),
                const SizedBox(height: 20),
                const Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(" Total balance in ",
                        style: TextStyle(color: Colors.black54, fontSize: 15)),
                    Text(
                      "USD",
                      style: TextStyle(color: Colors.blue, fontSize: 15),
                    )
                  ],
                ),
                const SizedBox(height:15),
                const Text("\$34,151.37",style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold),),
                const SizedBox(height: 8,),
                const Row(
                  children: [
                    Icon(Icons.trending_up_rounded,color: Colors.green,),
                    Text("+\$562.12",style: TextStyle(fontSize: 17,color:Colors.green),),
                    Text(" total",style: TextStyle(fontSize: 17),)
                  ],
                ),
                const SizedBox(height: 20,),
                Container(
                  height: 130,
                  child:Stack(
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                          image: DecorationImage(image: AssetImage("assets/images/crypto1.jpg"),fit: BoxFit.cover),
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(22),
                        child: const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment:MainAxisAlignment.spaceBetween ,
                              children: [
                                Text("Crypto Exchange",style: TextStyle(fontWeight: FontWeight.w500,fontSize: 20,color: Colors.white),),
                                CircleAvatar(maxRadius: 10,child: Icon(Icons.close,size: 15,color: Colors.black45),backgroundColor: Colors.white,)
                              ],
                            ),
                            SizedBox(height: 20,),
                            Text("Trusted by millions. Low fees.",style: TextStyle(color: Colors.white)),
                            SizedBox(height: 10,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Fastest trades. USD,EUR and GBP",style: TextStyle(color: Colors.white)),
                                Row(
                                children: [
                                  CircleAvatar(backgroundColor: Colors.white,maxRadius: 5,),
                                  SizedBox(width: 7,),
                                  CircleAvatar(backgroundColor: Colors.black45,maxRadius: 5,),
                                  SizedBox(width: 7,),
                                  CircleAvatar(backgroundColor: Colors.black45  ,maxRadius: 5,)
                                ],)
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Coins",style: TextStyle(fontWeight: FontWeight.w500,fontSize: 25),),
                    Icon(Icons.filter_list_rounded,color: Colors.black45,size: 30,)
                  ],
                ),
                //SizedBox(height: 10,),
                Expanded(
                  child: ListView.builder(
                    itemCount: cName.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 33),
                        child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text(cName[index], style: const TextStyle(color: Colors.black45)),
                                        const SizedBox(width: 5),
                                        Icon(cPercentageChange[index] > 0 ? Icons.trending_up_rounded : Icons.trending_down_rounded,color: cPercentageChange[index] > 0 ? Colors.green : Colors.red,),
                                        Text("${cPercentageChange[index].toStringAsFixed(2)}%", style: TextStyle(color: cPercentageChange[index] > 0 ? Colors.green : Colors.red)),
                                      ],
                                    ),
                                    const SizedBox(height: 2,),
                                    Row(
                                      children: [
                                        Text(cQuantity[index], style: const TextStyle(fontWeight: FontWeight.w700, fontSize: 19)),
                                        const SizedBox(width: 10),
                                        Text("~${cQuantityValues[index]}"),
                                      ],
                                    ),
                                  ],
                                ),
                                CircleAvatar(backgroundImage: AssetImage(cIcons[index]), maxRadius: 25),
                              ],
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 50,)
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Align(
            child: Material(
              elevation: 5,
              child: Container(
                padding: const EdgeInsets.only(top: 10,bottom: 10,right: 30,left: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Column(
                      children: [
                        Icon(CupertinoIcons.home,color: Colors.lightGreen),
                        SizedBox(height: 5,),
                        Text("Home")
                      ],
                    ),
                    const Column(
                      children: [
                        Icon(CupertinoIcons.chart_bar_alt_fill,color: Colors.black45),
                        SizedBox(height: 5,),
                        Text("Market",style: TextStyle(color: Colors.black45))
                      ],
                    ),
                    InkWell(
                      onTap: () => Navigator.pop(context),
                      child: const Column(
                        children: [
                          Icon(CupertinoIcons.add_circled,color: Colors.black45),
                          SizedBox(height: 5,),
                          Text("Actions",style: TextStyle(color: Colors.black45),)
                        ],
                      ),
                    ),
                    const Column(
                      children: [
                        Icon(CupertinoIcons.chat_bubble,color: Colors.black45),
                        SizedBox(height: 5,),
                        Text("Chat",style: TextStyle(color: Colors.black45))
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),)
        ],
      ),
    );
  }
}

List<dynamic> cName = ["Bitcoin","Ethereum","Chainink","Tether","Solana","Polkadot","Cardano"];
List<dynamic> cIcons = ["assets/images/bitcoin.png","assets/images/ethereum.png","assets/images/chainlink.png","assets/images/tether.png","assets/images/solana.png","assets/images/polkadot.png","assets/images/cardano.png"];
//List<dynamic> cStonks = [Icon(Icons.trending_down_rounded,color: Colors.red,),Icon(Icons.trending_up_rounded,color: Colors.green,),Icon(Icons.trending_up_rounded,color: Colors.green,),Icon(Icons.trending_up_rounded,color: Colors.green,),Icon(Icons.trending_down_rounded,color: Colors.red,),Icon(Icons.trending_up_rounded,color: Colors.green,),Icon(Icons.trending_up_rounded,color: Colors.red,),];
List<dynamic> cQuantity = ["0.07316 BTC","0.2321 ETH","1.3025 LINK","1.3025 USDT","2.1221 SOL","1.4352 DOT","0.7547 ADA"];
List<String> cQuantityValues = ["\$2,313.98", "\$3,146.22", "\$25.18", "\$1.01", "\$39.51", "\$32.88", "\$1.79"];
List<double> cPercentageChange = [-2.04, 3.12, 1.25, 0.75, -1.63, 2.88, -1.92];


