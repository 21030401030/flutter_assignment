import 'package:assignment/home.dart';
import 'package:assignment/crypto.dart';
import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    'https://img.freepik.com/premium-photo/wooden-cottage-surrounded-by-palm-trees-vegetable-garden-countryside-cabin-rainforest_677754-8218.jpg?w=740'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            color: Colors.black26.withOpacity(.5), // Dark overlay color
          ),
          Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(top: 100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        Text(
                          "Wanderly",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 35),
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        Text(
                          "Your Ultimate Companion for Seamless",
                          style: TextStyle(color: Colors.white),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Travel Experiences",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              const Flexible(
                  child: SizedBox(
                height: 475,
              )),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => const home()));
                },
                child: const Text(
                  "Sign in with Phone Number",
                ),
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.green[800],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 95, vertical: 17)),
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton.icon(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => const Crypto()));
                },
                icon: const Icon(
                  Icons.apple,
                  color: Colors.black,
                ),
                label: const Text(
                  "Sign in with Apple",
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 108, vertical: 13)),
              ),
              const SizedBox(height: 19),
              const Text(
                "Don't have an account? Sign Up",
                style: TextStyle(color: Colors.white),
              ),
              const SizedBox(height: 31),
              const Text(
                "By creating an account or signing in, you agree to",
                style: TextStyle(color: Colors.white),
              ),
              const SizedBox(height: 7.5),
              const Text(
                "our Terms of Service and Privacy Policyy",
                style: TextStyle(color: Colors.white),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ], //children
      ),
    );
  }
}
